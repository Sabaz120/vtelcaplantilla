<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Plantilla Vtelca</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<link rel="shortcut icon" href="images/favicon.ico">
		<link rel="stylesheet" href="plugins/theme-home/bootstrap/css/bootstrap.css">
		{{-- <link rel="stylesheet" href="plugins/theme-home/css/font-awesome.min.css"> --}}
		<link rel="stylesheet" href="plugins/theme-home/css/style.css">
		{{-- <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"> --}}
		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

		<link href="https://fonts.googleapis.com/css?family=Droid+Serif:400,400i,700,700i|Montserrat:200,200i,300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
<style media="screen">


</style>

	</head>
	<body>

		<section class="cover-1 text-center">
			<nav class="navbar navbar-expand-lg navbar-dark navbar-custom">
				<div class="container-fluid">
					<a class="navbar-brand" href="#">Plantilla Vtelca</a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>
					<div class="collapse navbar-collapse pull-xs-right justify-content-end"  id="navbarSupportedContent">
						<span class="btn btn-outline-white btn-outline"><strong >Contactos :</strong> Extensión 1900</span>

						</ul>
					</div>
				</div>
			</nav>
			<div class="cover-container pb-5">
				<div class="cover-inner container">
					<h1 class="jumbotron-heading"><strong>Bienvenido a nuestra	plantilla de Proyectos</strong></h1>
					{{-- <p class="lead text-danger"><strong>Una colección de elementos HTML y CSS codificados para ayudarlos el Home de su sitio web, totalmente responsive y basado en Bootstrap 4<</strong>/p> --}}
				</div>
			</div>
		</section>

		{{-- <section class="cover-2 text-left">
			<nav class="navbar navbar-expand-lg navbar-dark navbar-custom">
				<div class="container">
					<a class="navbar-brand" href="#">Plantilla Vtelca</a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>
					<div class="collapse navbar-collapse pull-xs-right justify-content-end" id="navbarSupportedContent">
						<ul class="navbar-nav mt-2 mt-md-0">
							<li class="nav-item active">
								<a class="nav-link" href="#">Inicio <span class="sr-only">(current)</span></a>
							</li>
							<li class="nav-item">
								<a class="btn btn-success pill-btn" href="#">Sistema Producción</a>

							</li>
							<li class="nav-item">
								<a class="nav-link" href="#">Siintra</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#">Sigesp</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#">Sala Situacional</a>
							</li>
						</ul>
					</div>
				</div>
			</nav>
			<div class="cover-container">
				<div class="cover-inner container">
						<div class="row justify-center">
							<div class="col-lg-5 mt-5 mb-5 mr-5 text-center text-lg-left">
								<h1 class="jumbotron-heading">Plantilla de Proyectos</h1>
								<p class="lead">Una colección de elementos HTML y CSS codificados para ayudarlos el Home de su sitio web, totalmente responsive y basado en Bootstrap 4</p>
						</div>
						<div class="col-lg-5 offset-lg-2 mt-5 mb-5 text-center ml-5 pb-5">
							<div id="login-box">
								<div class="logo">
									<h1 class="logo-caption"><span class="tweak">I</span>niciar Sesión</h1>
								</div>
								<div class="controls">
									<input type="text" name="username" placeholder="Username" class="form-control" />
									<input type="text" name="username" placeholder="Password" class="form-control" />
									<button type="button" class="btn btn-default btn-block btn-custom">Acceder</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section> --}}


		<section class="cta-4 text-center justify-center">
			<div class="container">
				<h2 class="mb-5">Accede a los sistemas de la fabrica</h2>
				<p class="lead">Una colección de elementos HTML y CSS codificados para ayudarlos el Home de su sitio web, totalmente responsive y basado en Bootstrap 4</p>
				<div class="divider"></div>
				<form class="subscribe" method="POST" action="{{ route('login') }}">
					 {{ csrf_field() }}
					<div class="form-group row pt-4 pb-2 m-auto col-md-8">
						<div class="col-md-6 mb-3{{ $errors->has('email') ? ' has-error' : '' }}">
							<input type="email" class="form-control-custom" id="email" name="email" placeholder="Usuario">
							@if ($errors->has('email'))
								<span class="help-block">
									<strong>{{ $errors->first('email') }}</strong>
								</span>
							@endif
						</div>
						<div class="col-md-6 mb-3{{ $errors->has('password') ? ' has-error' : '' }}">
							<input type="password" class="form-control-custom" id="password" name="password" placeholder="Contraseña">
							@if ($errors->has('password'))
								<span class="help-block">
									<strong>{{ $errors->first('password') }}</strong>
								</span>
							@endif
						</div>
						<div class="col-md-6 mb-3 mx-auto">
							<select class="form-control-custom text-center" name="sistemas" required>
									<option value="">Seleccione el sistema </option>
									<option value="1">Sistemas Producción</option>
									<option value="2">Siintra</option>
									<option value="3">Sala Situacional</option>
							</select>
						</div>
					</div>

					<div class="row">
						<div class="m-auto col-md-4">
							<button type="submit" class="btn btn-lg btn-primary pill-btn">Accceder</button>
						</div>
					</div>
				</form>
			</div>
		</section>






<footer>
     <div class="container-fluid bg-primary py-3">
    <div class="container">
      <div class="row">
        <div class="col-md-7">
            <div class="row py-0">
          <div class="col-sm-1 hidden-md-down">
              {{-- <a class="bg-circle bg-info" href="#">
                <i class="fa fa-2x fa-fw fa-address-card" aria-hidden="true "></i>
              </a> --}}
            </div>
            <div class="col-sm-11 text-white">
                <div><h4> VTELCA C.A</h4>
                    <p> <span class="">Venezolana</span>de<span class="">Telecomunicaciones</span></p>
                </div>
            </div>
            </div>
        </div>
        <div class="col-md-5">
          <div class="d-inline-block">
            <div class="bg-circle-outline d-inline-block" style="background-color:#3b5998">
              <a href="https://www.facebook.com/"><i class="fa fa-2x fa-fw fa-facebook text-white"></i>
		</a>
            </div>
            <div class="bg-circle-outline d-inline-block" style="background-color:#4099FF">
              <a href="https://twitter.com/">
                <i class="fa fa-2x fa-fw fa-twitter text-white"></i></a>
            </div>

            <div class="bg-circle-outline d-inline-block" style="background-color:#0077B5">
              <a href="https://www.linkedin.com/company/">
                <i class="fa fa-2x fa-fw fa-linkedin text-white"></i></a>
            </div>
            <div class="bg-circle-outline d-inline-block" style="background-color:#d34836">
              <a href="https://www.google.com/">
                <i class="fa fa-2x fa-fw fa-google text-white"></i></a>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
</footer>


		<!-- Placed at the end of the document so the pages load faster -->
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
		<script src="bootstrap/js/bootstrap.min.js"></script>

	</body>
</html>
