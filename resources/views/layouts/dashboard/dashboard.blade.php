<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Venezolana de Telecomunicaciones C.A. (VTELCA)</title>
    <link rel="icon" href="{{url('favicon.ico')}}" type="image/x-icon">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Colorpicker Css -->
    <link href="plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css" rel="stylesheet" />

    <!-- Dropzone Css -->
    <link href="plugins/dropzone/dropzone.css" rel="stylesheet">

    <!-- Multi Select Css -->
    <link href="plugins/multi-select/css/multi-select.css" rel="stylesheet">

    <!-- Bootstrap Spinner Css -->
    <link href="../../plugins/jquery-spinner/css/bootstrap-spinner.css" rel="stylesheet">

    <!-- Bootstrap Tagsinput Css -->
    <link href="plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet">

    <!-- Bootstrap Select Css -->
    <link href="plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

    <!-- noUISlider Css -->
    <link href="plugins/nouislider/nouislider.min.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="css/style.css" rel="stylesheet">

    <link href="css/themes/all-themes.css" rel="stylesheet" />
    <style media="screen">
    .navbar {
      /*Color de fondo del navbar donde esta la barra de busqueda*/
      background-color:#F44336;
    }
    .navbar .navbar-brand {
      /*Color de letra del navbar*/
      color:#fff
    }

    .nav > li > a {
      color: #fff; }

    </style>

    @stack('styles')
  </head>
  <body>
    <div class="container-fluid">
      <!-- Inicio Cabecera (Barra de búsqueda y notificaciones) -->
      @include('layouts.dashboard.partials.header')
      <!-- Fin Cabecera (Barra de búsqueda y notificaciones) -->

      <!-- Inicio Barra lateral izquierda (Menú de opciones del sistema)  -->
      @include('layouts.dashboard.partials.leftSideBar')
      <!-- Fin barra lateral izquierda -->

      <!-- Inicio contenido   -->
      @yield('contenido')
      <!-- Fin contenido -->

      @include('layouts.dashboard.partials.footer')
      <script src="plugins/jquery/jquery.min.js"></script>

      <!-- Bootstrap Core Js -->
      <script src="plugins/bootstrap/js/bootstrap.js"></script>

      <!-- Select Plugin Js -->
      <script src="plugins/bootstrap-select/js/bootstrap-select.js"></script>

      <!-- Slimscroll Plugin Js -->
      <script src="plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

      <!-- Bootstrap Colorpicker Js -->
      <script src="plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>

      <!-- Dropzone Plugin Js -->
      <script src="plugins/dropzone/dropzone.js"></script>

      <!-- Input Mask Plugin Js -->
      <script src="plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script>

      <!-- Multi Select Plugin Js -->
      <script src="plugins/multi-select/js/jquery.multi-select.js"></script>

      <!-- Jquery Spinner Plugin Js -->
      <script src="plugins/jquery-spinner/js/jquery.spinner.js"></script>

      <!-- Bootstrap Tags Input Plugin Js -->
      <script src="plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

      <!-- noUISlider Plugin Js -->
      <script src="plugins/nouislider/nouislider.js"></script>

      <!-- Waves Effect Plugin Js -->
      <script src="plugins/node-waves/waves.js"></script>

      <!-- Custom Js -->
      <script src="js/admin.js"></script>
      <script src="js/pages/index.js"></script>
      <!-- Demo Js -->
      <script src="js/demo.js"></script>

      

    </div>
    @stack('script')
  </body>
</html>
