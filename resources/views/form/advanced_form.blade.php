@extends('layouts.dashboard.dashboard')
@section('contenido')
  <section class="content">
          <div class="container-fluid">
              <div class="block-header">
                  <h2>ADVANCED FORM ELEMENTS</h2>
              </div>
              
              <!-- File Upload | Drag & Drop OR With Click & Choose -->
              <div class="row clearfix">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <div class="card">
                          <div class="header">
                              <h2>
                                  FILE UPLOAD - DRAG & DROP OR WITH CLICK & CHOOSE
                                  <small>Taken from <a href="http://www.dropzonejs.com/" target="_blank">www.dropzonejs.com</a></small>
                              </h2>
                              <ul class="header-dropdown m-r--5">
                                  <li class="dropdown">
                                      <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                          <i class="material-icons">more_vert</i>
                                      </a>
                                      <ul class="dropdown-menu pull-right">
                                          <li><a href="javascript:void(0);">Action</a></li>
                                          <li><a href="javascript:void(0);">Another action</a></li>
                                          <li><a href="javascript:void(0);">Something else here</a></li>
                                      </ul>
                                  </li>
                              </ul>
                          </div>
                          <div class="body">
                              <form action="/" id="frmFileUpload" class="dropzone" method="post" enctype="multipart/form-data">
                                  <div class="dz-message">
                                      <div class="drag-icon-cph">
                                          <i class="material-icons">touch_app</i>
                                      </div>
                                      <h3>Drop files here or click to upload.</h3>
                                      <em>(This is just a demo dropzone. Selected files are <strong>not</strong> actually uploaded.)</em>
                                  </div>
                                  <div class="fallback">
                                      <input name="file" type="file" multiple />
                                  </div>
                              </form>
                          </div>
                      </div>
                  </div>
              </div>
              <!-- #END# File Upload | Drag & Drop OR With Click & Choose -->

              <!-- Input Group -->
              <div class="row clearfix">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <div class="card">
                          <div class="header">
                              <h2>
                                  INPUT GROUP
                              </h2>
                              <ul class="header-dropdown m-r--5">
                                  <li class="dropdown">
                                      <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                          <i class="material-icons">more_vert</i>
                                      </a>
                                      <ul class="dropdown-menu pull-right">
                                          <li><a href="javascript:void(0);">Action</a></li>
                                          <li><a href="javascript:void(0);">Another action</a></li>
                                          <li><a href="javascript:void(0);">Something else here</a></li>
                                      </ul>
                                  </li>
                              </ul>
                          </div>
                          <div class="body">
                              <h2 class="card-inside-title">With Icon</h2>
                              <div class="row clearfix">
                                  <div class="col-md-4">
                                      <div class="input-group">
                                          <span class="input-group-addon">
                                              <i class="material-icons">person</i>
                                          </span>
                                          <div class="form-line">
                                              <input type="text" class="form-control date" placeholder="Username">
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-md-4">
                                      <div class="input-group">
                                          <div class="form-line">
                                              <input type="text" class="form-control date" placeholder="Message">
                                          </div>
                                          <span class="input-group-addon">
                                              <i class="material-icons">send</i>
                                          </span>
                                      </div>
                                  </div>
                                  <div class="col-md-4">
                                      <div class="input-group">
                                          <span class="input-group-addon">
                                              <i class="material-icons">person</i>
                                          </span>
                                          <div class="form-line">
                                              <input type="text" class="form-control date" placeholder="Recipient's username">
                                          </div>
                                          <span class="input-group-addon">
                                              <i class="material-icons">send</i>
                                          </span>
                                      </div>
                                  </div>
                              </div>

                              <h2 class="card-inside-title">With Text</h2>
                              <div class="row clearfix">
                                  <div class="col-md-4">
                                      <div class="input-group">
                                          <span class="input-group-addon">@</span>
                                          <div class="form-line">
                                              <input type="text" class="form-control" placeholder="Username">
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-md-4">
                                      <div class="input-group">
                                          <div class="form-line">
                                              <input type="text" class="form-control" placeholder="Recipient's username">
                                          </div>
                                          <span class="input-group-addon">@example.com</span>
                                      </div>
                                  </div>
                                  <div class="col-md-4">
                                      <div class="input-group">
                                          <span class="input-group-addon">$</span>
                                          <div class="form-line">
                                              <input type="text" class="form-control date">
                                          </div>
                                          <span class="input-group-addon">.00</span>
                                      </div>
                                  </div>
                              </div>

                              <h2 class="card-inside-title">
                                  Different Sizes
                                  <small>You can use the <code>.input-group-sm, .input-group-lg</code> classes for sizing.</small>
                              </h2>
                              <div class="row clearfix">
                                  <div class="col-md-4">
                                      <p>
                                          <b>Input Group Large</b>
                                      </p>
                                      <div class="input-group input-group-lg">
                                          <span class="input-group-addon">
                                              <i class="material-icons">person</i>
                                          </span>
                                          <div class="form-line">
                                              <input type="text" class="form-control" placeholder="Username">
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-md-4">
                                      <p>
                                          <b>Input Group Default</b>
                                      </p>
                                      <div class="input-group">
                                          <div class="form-line">
                                              <input type="text" class="form-control" placeholder="Message">
                                          </div>
                                          <span class="input-group-addon">
                                              <i class="material-icons">send</i>
                                          </span>
                                      </div>
                                  </div>
                                  <div class="col-md-4">
                                      <p>
                                          <b>Input Group Small</b>
                                      </p>
                                      <div class="input-group input-group-sm">
                                          <span class="input-group-addon">
                                              <i class="material-icons">person</i>
                                          </span>
                                          <div class="form-line">
                                              <input type="text" class="form-control" placeholder="Recipient's username">
                                          </div>
                                          <span class="input-group-addon">
                                              <i class="material-icons">send</i>
                                          </span>
                                      </div>
                                  </div>
                              </div>
                              <div class="row clearfix">
                                  <div class="col-md-4">
                                      <div class="input-group input-group-lg">
                                          <span class="input-group-addon">@</span>
                                          <div class="form-line">
                                              <input type="text" class="form-control" placeholder="Username">
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-md-4">
                                      <div class="input-group">
                                          <div class="form-line">
                                              <input type="text" class="form-control" placeholder="Recipient's username">
                                          </div>
                                          <span class="input-group-addon">@example.com</span>
                                      </div>
                                  </div>
                                  <div class="col-md-4">
                                      <div class="input-group input-group-sm">
                                          <span class="input-group-addon">$</span>
                                          <div class="form-line">
                                              <input type="text" class="form-control">
                                          </div>
                                          <span class="input-group-addon">.00</span>
                                      </div>
                                  </div>
                              </div>

                              <h2 class="card-inside-title">Radio & Checkbox</h2>
                              <div class="row clearfix">
                                  <div class="col-md-6">
                                      <div class="input-group input-group-lg">
                                          <span class="input-group-addon">
                                              <input type="checkbox" class="filled-in" id="ig_checkbox">
                                              <label for="ig_checkbox"></label>
                                          </span>
                                          <div class="form-line">
                                              <input type="text" class="form-control">
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-md-6">
                                      <div class="input-group input-group-lg">
                                          <span class="input-group-addon">
                                              <input type="radio" class="with-gap" id="ig_radio">
                                              <label for="ig_radio"></label>
                                          </span>
                                          <div class="form-line">
                                              <input type="text" class="form-control">
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <!-- #END# Input Group -->


              <div class="row clearfix">
                  <!-- Spinners -->
                  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                      <div class="card">
                          <div class="header">
                              <h2>
                                  SPINNERS
                                  <small>Taken from <a href="https://github.com/vsn4ik/jquery.spinner" target="_blank">github.com/vsn4ik/jquery.spinner</a></small>
                              </h2>
                              <ul class="header-dropdown m-r--5">
                                  <li class="dropdown">
                                      <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                          <i class="material-icons">more_vert</i>
                                      </a>
                                      <ul class="dropdown-menu pull-right">
                                          <li><a href="javascript:void(0);">Action</a></li>
                                          <li><a href="javascript:void(0);">Another action</a></li>
                                          <li><a href="javascript:void(0);">Something else here</a></li>
                                      </ul>
                                  </li>
                              </ul>
                          </div>
                          <div class="body">
                              <div class="row clearfix">
                                  <div class="col-md-6">
                                      <div class="input-group spinner" data-trigger="spinner">
                                          <div class="form-line">
                                              <input type="text" class="form-control text-center" value="1" data-rule="quantity">
                                          </div>
                                          <span class="input-group-addon">
                                              <a href="javascript:;" class="spin-up" data-spin="up"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                              <a href="javascript:;" class="spin-down" data-spin="down"><i class="glyphicon glyphicon-chevron-down"></i></a>
                                          </span>
                                      </div>
                                  </div>
                                  <div class="col-md-6">
                                      <div class="input-group spinner" data-trigger="spinner">
                                          <div class="form-line">
                                              <input type="text" class="form-control text-center" value="1" data-rule="currency">
                                          </div>
                                          <span class="input-group-addon">
                                              <a href="javascript:;" class="spin-up" data-spin="up"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                              <a href="javascript:;" class="spin-down" data-spin="down"><i class="glyphicon glyphicon-chevron-down"></i></a>
                                          </span>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
                  <!-- #END# Spinners -->
                  <!-- Tags Input -->
                  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                      <div class="card">
                          <div class="header">
                              <h2>
                                  TAGS INPUT
                                  <small>Taken from <a href="https://github.com/bootstrap-tagsinput/bootstrap-tagsinput" target="_blank">github.com/bootstrap-tagsinput/bootstrap-tagsinput</a></small>
                              </h2>
                              <ul class="header-dropdown m-r--5">
                                  <li class="dropdown">
                                      <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                          <i class="material-icons">more_vert</i>
                                      </a>
                                      <ul class="dropdown-menu pull-right">
                                          <li><a href="javascript:void(0);">Action</a></li>
                                          <li><a href="javascript:void(0);">Another action</a></li>
                                          <li><a href="javascript:void(0);">Something else here</a></li>
                                      </ul>
                                  </li>
                              </ul>
                          </div>
                          <div class="body">
                              <div class="form-group demo-tagsinput-area">
                                  <div class="form-line">
                                      <input type="text" class="form-control" data-role="tagsinput" value="Amsterdam,Washington,Sydney,Beijing,Cairo">
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
                  <!-- #END# Tags Input -->
              </div>
              <!-- Advanced Select -->
              <div class="row clearfix">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <div class="card">
                          <div class="header">
                              <h2>
                                  ADVANCED SELECT
                                  <small>Taken from <a href="https://silviomoreto.github.io/bootstrap-select/" target="_blank">silviomoreto.github.io/bootstrap-select</a></small>
                              </h2>
                              <ul class="header-dropdown m-r--5">
                                  <li class="dropdown">
                                      <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                          <i class="material-icons">more_vert</i>
                                      </a>
                                      <ul class="dropdown-menu pull-right">
                                          <li><a href="javascript:void(0);">Action</a></li>
                                          <li><a href="javascript:void(0);">Another action</a></li>
                                          <li><a href="javascript:void(0);">Something else here</a></li>
                                      </ul>
                                  </li>
                              </ul>
                          </div>
                          <div class="body">
                              <div class="row clearfix">
                                  <div class="col-md-3">
                                      <p>
                                          <b>Basic</b>
                                      </p>
                                      <select class="form-control show-tick">
                                          <option>Mustard</option>
                                          <option>Ketchup</option>
                                          <option>Relish</option>
                                      </select>

                                  </div>
                                  <div class="col-md-3">
                                      <p>
                                          <b>With OptGroups</b>
                                      </p>
                                      <select class="form-control show-tick">
                                          <optgroup label="Picnic">
                                              <option>Mustard</option>
                                              <option>Ketchup</option>
                                              <option>Relish</option>
                                          </optgroup>
                                          <optgroup label="Camping">
                                              <option>Tent</option>
                                              <option>Flashlight</option>
                                              <option>Toilet Paper</option>
                                          </optgroup>
                                      </select>

                                  </div>
                                  <div class="col-md-3">
                                      <p>
                                          <b>Multiple Select</b>
                                      </p>
                                      <select class="form-control show-tick" multiple>
                                          <option>Mustard</option>
                                          <option>Ketchup</option>
                                          <option>Relish</option>
                                      </select>

                                  </div>
                                  <div class="col-md-3">
                                      <p>
                                          <b>With Search Bar</b>
                                      </p>
                                      <select class="form-control show-tick" data-live-search="true">
                                          <option>Hot Dog, Fries and a Soda</option>
                                          <option>Burger, Shake and a Smile</option>
                                          <option>Sugar, Spice and all things nice</option>
                                      </select>
                                  </div>
                              </div>
                              <div class="row clearfix">
                                  <div class="col-md-3">
                                      <p>
                                          <b>Max Selection Limit: 2</b>
                                      </p>
                                      <select class="form-control show-tick" multiple>
                                          <optgroup label="Condiments" data-max-options="2">
                                              <option>Mustard</option>
                                              <option>Ketchup</option>
                                              <option>Relish</option>
                                          </optgroup>
                                          <optgroup label="Breads" data-max-options="2">
                                              <option>Plain</option>
                                              <option>Steamed</option>
                                              <option>Toasted</option>
                                          </optgroup>
                                      </select>
                                  </div>
                                  <div class="col-md-3">
                                      <p>
                                          <b>Display Count</b>
                                      </p>
                                      <select class="form-control show-tick" multiple data-selected-text-format="count">
                                          <option>Mustard</option>
                                          <option>Ketchup</option>
                                          <option>Relish</option>
                                          <option>Onions</option>
                                      </select>
                                  </div>
                                  <div class="col-md-3">
                                      <p>
                                          <b>With SubText</b>
                                      </p>
                                      <select class="form-control show-tick" data-show-subtext="true">
                                          <option data-subtext="French's">Mustard</option>
                                          <option data-subtext="Heinz">Ketchup</option>
                                          <option data-subtext="Sweet">Relish</option>
                                          <option data-subtext="Miracle Whip">Mayonnaise</option>
                                          <option data-divider="true"></option>
                                          <option data-subtext="Honey">Barbecue Sauce</option>
                                          <option data-subtext="Ranch">Salad Dressing</option>
                                          <option data-subtext="Sweet &amp; Spicy">Tabasco</option>
                                          <option data-subtext="Chunky">Salsa</option>
                                      </select>
                                  </div>
                                  <div class="col-md-3">
                                      <p>
                                          <b>Disabled Option</b>
                                      </p>
                                      <select class="form-control show-tick">
                                          <option>Mustard</option>
                                          <option disabled>Ketchup</option>
                                          <option>Relish</option>
                                      </select>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <!-- #END# Advanced Select -->
          </div>
      </section>



@endsection
